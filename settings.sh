INTERNAL_VERSION=1.3.0
SLIDEV_VERSION=0.42.4

DOCKER_TAG_LATEST=latest
DOCKER_TAG_CURRENT=$INTERNAL_VERSION-$SLIDEV_VERSION
DOCKER_TAG_INTERNAL=$INTERNAL_VERSION

CONTAINERNAME=slidev
IMAGENAME=ekleinod/$CONTAINERNAME
DOCKER_REGISTRY=docker.io
