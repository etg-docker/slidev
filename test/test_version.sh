source ../settings.sh

echo "Shows version."
echo

docker run \
	--rm \
	--interactive \
	--name $CONTAINERNAME \
	--init \
	$IMAGENAME \
		--version
