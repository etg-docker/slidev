source ../settings.sh

echo "Empty call - shows help."
echo

docker run \
	--rm \
	--interactive \
	--name $CONTAINERNAME \
	--init \
	$IMAGENAME
