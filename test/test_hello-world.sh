source ../settings.sh

echo "Exporting hello world to pdf."
echo

docker run \
	--rm \
	--interactive \
	--name $CONTAINERNAME \
	--volume "${PWD}:/slidev" \
	--publish 3030:3030 \
	--init \
	$IMAGENAME \
		export hello-world.md
		# hello-world.md --remote
