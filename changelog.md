# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- `slidev-pdf` script for exporting pdf
- `slidev-present` script for calling present mode


## [1.3.0-0.42.4] 2023-06-16

### Changed

- updated slidev to 0.42.4


## [1.2.0-0.40.10] 2023-04-17

### Changed

- updated slidev to 0.40.10


## [1.1.0-0.36.5] 2022-10-03

### Added

- material design icons

### Changed

- updated slidev to 0.36.5


## [1.0.0-0.35.4] 2022-08-12

- initial version

### Added

- test files
- CI/CD for gitlab
- official themes
	- apple-basic
	- bricks
	- default
	- seriph
	- shibainu
- community themes
	- academic
